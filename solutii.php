<?php 
	include "session.php";
	if (!isset($_SESSION["user"]) || !isset($_SESSION["uid"])) {
		echo "<h1>Pagina inexistenta!</h1>";
		exit;
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Infomania Solutii <?php if (isset($_GET["nume"])) echo $_GET["nume"]; else echo "Inexistente"?></title>
		<?php include "include\\head.html" ?>
	</head>
	<body class="metro">
		<?php include "include\\navbar.html" ?>
		<div class="metro container" name="principal">
			<?php 
				if (!isset($_GET["pid"]) || !isset($_GET["nume"])) {
					echo "<h1>Solutii inexistente</h1>";
					exit;
				}
				require_once("db.php");
				try {
					$db = new PDO("mysql:host=$hostdb; dbname=$namedb", $userdb, $passdb);
					$prep = $db->prepare("SELECT sid, date_time, score FROM users_problems WHERE uid = :uid && pid = :pid
										ORDER BY date_time DESC");

					if ($prep->execute(array("uid" => $_SESSION["uid"], "pid" => $_GET["pid"])) === false) {
						echo "<h1>Ceva nu a mers bine, reincercati!</h1>";
						exit;
					}

					if ($prep->rowCount() <= 0) {
						echo "<h1>Inca nu ati rezolvat aceasta problema!</h1>";
						exit;
					}
				}
				catch (PDOException $e) {
					echo "<h1>Nu s-a reusit conectarea la baza de date: " . $e->getMessage() . "</h1>";
					exit;
				}
				$count = 1;
			?>
			<h1>Solutiile lui <?php echo $_SESSION["user"]; ?></h1>
			<table class="table bordered hovered">
				<thead>
					<th>Nr.</th>
					<th>Utlizator</th>
					<th>Problema</th>
					<th>Data</th>
					<th>Scor</th>
				</thead>
				<tbody>
					<?php while ($row = $prep->fetch()) : ?>
						<tr class="clickableRow" href='<?php echo "solutie.php?sid=" . $row["sid"];?>'>
							<td><?php echo $count; $count++; ?></td>
							<td><a href='<?php echo "profil.php?user=" . $_SESSION["user"]; ?>'><?php echo $_SESSION["user"];
							?></a></td>
							<td><?php echo $_GET["nume"]; ?></td>
							<td><?php echo $row["date_time"] ?></td>
							<td><?php echo $row["score"] ?></td>
						</tr>
					<?php endwhile; ?>
				</tbody>
			</table>
		</div>
		<script type="text/javascript" src="public/javascripts/clickableRow.js"></script>
	</body>
</html>