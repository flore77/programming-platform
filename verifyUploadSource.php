<?php
	require_once("db.php");

	function dateTime () {
		date_default_timezone_set('Europe/Bucharest');
		$date_time = date("y") . "-" . date("m") . "-" . date("d") . " " .date("H") . ":" . date("i") . ":" . date("s");
		return $date_time;
	}

	function inDatabase ($text, $file) {
		global $hostdb, $namedb, $userdb, $passdb;
		try {
			$db = new PDO("mysql:host=$hostdb; dbname=$namedb", $userdb, $passdb);
			$prep = $db->prepare("INSERT INTO users_problems (uid, pid, date_time, score, test1, test2, test3, test4,
								test5, test6, test7, test8, test9, test10, message, file) VALUES (:uid, :pid, :date_time,
								'0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', :text, :file)");
			$date_time = dateTime();
			if ($prep->execute(array(":uid" => $_POST["uid"], ":pid" => $_POST["pid"], ":date_time" => $date_time,
				":text" => $text, ":file" => $file)) === false) {
				header('HTTP/1.1 500 ' . "Ceva nu a mers bine, reincercati!");
				header('Content-Type: application/json; charset=UTF-8');
			}

		}
		catch (PDOException $e) {
			header('HTTP/1.1 500 ' . "Nu s-a reusit conectarea la baza de date: " . $e->getMessage());
	   		header('Content-Type: application/json; charset=UTF-8');
		}
	}

	if ($_FILES["sourceUpload"]["error"] == 0) {
		$info = pathinfo($_FILES["sourceUpload"]["name"]);
		if ($info["extension"] === "cpp" || $info["extension"] === "c") {
			$folder = "Uploads\\" . $_POST["uid"];
			if (is_dir($folder) === false) {
				mkdir($folder);
			}
			$folder .= "\\" . $_POST["pid"];
			if (is_dir($folder) === false) {
				mkdir($folder);
			}
			$uploadPath = $folder . "\\" . $info["basename"];
			$aux = 1;
			while (file_exists($uploadPath)) {
				$uploadPath = $folder . "\\" . $info["filename"] . $aux . "." . $info["extension"];
				$aux++;
			}
			if ($aux != 1)
				$file = $info["filename"] . ($aux - 1) . "." . $info["extension"];
			else
				$file = $info["basename"];
			if (move_uploaded_file($_FILES["sourceUpload"]["tmp_name"], $uploadPath)) {
				chdir("Source");
				$cmd = "compileEval " . $file . " " . $_POST["file"] . " " . $info["extension"] . " " . $_POST["uid"] . " "
				. $_POST["pid"];
				shell_exec($cmd);
				if (file_exists("compileError.txt")) {
					$f = fopen("compileError.txt", "r");
					if ($f) {
						$text = fread($f, filesize("compileError.txt"));
						inDatabase($text, $file);
						echo $text;
						fclose($f);
					}
					else {
						header('HTTP/1.1 500 ' . "Ceva nu a mers bine, reincercati!");
				        header('Content-Type: application/json; charset=UTF-8');
					}
					unlink("compileError.txt");
				}
				if (file_exists("file.txt")) {
					$f = fopen("file.txt", "r");
					if ($f) {
						$text = fread($f, filesize("file.txt"));
						inDatabase($text, $file);
						echo $text;
						fclose($f);
					}
					else {
						header('HTTP/1.1 500 ' . "Ceva nu a mers bine, reincercati!");
				        header('Content-Type: application/json; charset=UTF-8');
					}
					unlink("file.txt");
				}
				if (file_exists("result.txt")) {
					$f = fopen("result.txt", "r");
					if ($f) {
						$score = 0;
						$test = array();
						$index = 0;
						while (!feof($f)) {
							$linie = fgets($f, 4);
							$test[$index] = (int) $linie;;
							$score += $test[$index];
							$index++;
						}

						try {
							$db = new PDO("mysql:host=$hostdb; dbname=$namedb", $userdb, $passdb);
							$prep = $db->prepare("INSERT INTO users_problems (uid, pid, date_time, score, test1, test2,
								test3, test4, test5, test6, test7, test8, test9, test10, file) VALUES (:uid, :pid,
								:date_time, :score, :test1, :test2, :test3, :test4, :test5, :test6, :test7, :test8, :test9,
								:test10, :file)");
							
							$date_time = dateTime();

							if ($prep->execute(array(":uid" => $_POST["uid"], ":pid" => $_POST["pid"],
								":date_time" => $date_time, ":score" => $score, ":test1" => $test[0], ":test2" => $test[2],
								":test3" => $test[4], ":test4" => $test[6], ":test5" => $test[8], ":test6" => $test[10],
								":test7" => $test[12], ":test8" => $test[14], ":test9" => $test[16],
								":test10" => $test[18], ":file" => $file)) === false) {
									header('HTTP/1.1 500 ' . "Ceva nu a mers bine, reincercati!");
									header('Content-Type: application/json; charset=UTF-8');
							}
							echo $score;
				
						}
						catch (PDOException $e) {
							header('HTTP/1.1 500 ' . "Nu s-a reusit conectarea la baza de date: " . $e->getMessage());
				       		header('Content-Type: application/json; charset=UTF-8');
						}
						fclose($f);
					}
					else {
						header('HTTP/1.1 500 ' . "Ceva nu a mers bine, reincercati!");
				        header('Content-Type: application/json; charset=UTF-8');
					}
					unlink("result.txt");
				}
				chdir("..");
			}
			else {
				header('HTTP/1.1 500 ' . "Ceva nu a mers bine, reincercati!");
				header('Content-Type: application/json; charset=UTF-8');
			}
		}
		else {
			echo "Format invalid!";
		}
	}
	else {
		echo "Serverul nu a reusit sa salveze fisierul, reincarcati fisierul!";
	}	
?>