<?php
	$hostdb = 'localhost';
	$namedb = 'infomania';
	$userdb = 'root';
	$passdb = '';

	try {
		$db = new PDO("mysql:host=$hostdb; dbname=$namedb", $userdb, $passdb);
		$prep = $db->prepare("UPDATE users SET lastname = :lastname, firstname = :firstname, study = :study,
		 city = :city, show_email = :show_email, description = :description WHERE username = :user");
		if($prep->execute(array(":firstname" => $_POST["firstname"], ":lastname" => $_POST["lastname"], 
			":study" => $_POST["study"], ":city" => $_POST["city"], ":show_email" => $_POST["radio"], 
			":description" => $_POST["description"], ":user" => $_POST["user"])) === false) {
			echo "Ceva nu a mers bine, reincercati!";
			exit;
		}
		echo "Profilul dvs. s-a acutalizat cu succes!";
	}
	catch (PDOException $e) {
		echo "Nu s-a reusit conectarea la baza de date: " . $e->getMessage();
		exit;
	}

	$aux = $_POST["user"];
	$result = $db->query("SELECT uid FROM users WHERE username = '$aux' ");
	if ($result === false) {
		echo "Ceva nu a mers bine, reincercati!";
		exit;
	}
	else {
		$row = $result->fetch();
	}

	if ($_FILES["profilePicture"]["error"] == 0) {
		$info = pathinfo($_FILES["profilePicture"]["name"]);
		if ($info["extension"] === "jpeg" || $info["extension"] === "jpg") {
			$folder =  "Uploads\\" . $row["uid"];
			if (is_dir($folder) === false)
				mkdir($folder);
			$folder .= "\\profile";
			if (is_dir($folder) === false)
				mkdir($folder);
			$uploadPath = $folder . "\\profile_picture" . $row["uid"] .".jpg";
			if (move_uploaded_file($_FILES["profilePicture"]["tmp_name"], $uploadPath)) {}
			else {
				echo "Nu s-a reusit actualizarea pozei de profil, reincercati!";
			}
		}
		else {
			echo "Format invalid!";
		}
	}
	else if ($_FILES["profilePicture"]["error"] != 4) {
		echo "Nu s-a reusit actualizare fotografiei de profil, reincercati!";
	}
?>