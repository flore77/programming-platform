@echo off 
if [%1]==[] (
	exit /b 1
)
if [%2]==[] (
	exit /b 2
)
if [%3]==[] (
	exit /b 3
)
if [%4]==[] (
	exit /b 4
)
if %3==cpp (
	g++ ..\Uploads\%4\%5\%1 -o program 2> compileError.txt
)
if %3==c (
	gcc ..\Uploads\%4\%5\%1 -o program 2> compileError.txt
)
if %errorlevel%==0 (
del compileError.txt
mkdir Output
move "program.exe" "Output" > nul
cd Output
setlocal EnableDelayedExpansion
set /a number=0
for %%i in (..\%5\Input\*.in) do ( 
	type %%i >> %2.in
	program.exe
	if not exist %2.out (
		cd ..
		echo Output file is missing > file.txt
		rmdir /s /q Output
		exit /b 5
	)
	for %%j in (%2.out) do type %%j > %2!number!.out
	set /a number+=1
	type nul > %2.in
)
del %2.out
del %2.in
del program.exe
cd ..
for /l %%i in (0 1 9) do (
	fc /w %5\Grader\%2%%i.ok Output\%2%%i.out > nul
	if !errorlevel!==0 (
		echo 10 >> result.txt
	) else (
		echo 0 >> result.txt
	)
)
rmdir /s /q Output
exit /b 0
) else (
	exit /b 4
)