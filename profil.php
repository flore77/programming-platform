<?php include "session.php" ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Infomania Profil <?php if (isset($_GET["user"])) echo $_GET["user"]; else echo "Inexistent" ?></title>
		<?php include "include\\head.html" ?>
	</head>
	<body class="metro">
		<?php include "include\\navbar.html" ?>
		<div class="metro container">
			<?php 
				if (!isset($_GET["user"])) {
					echo "<h1>Acest utilizator nu exista!<h1>";
					exit;
				}
				if (!isset($_SESSION["user"])) {
					$ok = 0;
				}
				else if ($_SESSION["user"] === $_GET["user"]) {
					$ok = 1;
				}
				else {
					$ok = 0;
				}

				$hostdb = 'localhost';
				$namedb = 'infomania';
				$userdb = 'root';
				$passdb = '';

				try {
					$db = new PDO("mysql:host=$hostdb; dbname=$namedb", $userdb, $passdb);
					$prep = $db->prepare("SELECT * FROM users WHERE username = :username");
					if ($prep->execute(array("username" => $_GET["user"])) === false) {
						echo "<h1>Ceva nu a mers bine, reincercati!</h1>";
						exit;
					}
					if ($prep->rowCount() <= 0) {
						echo "<h1>Acest utilizator nu exista!<h1>";
						exit;
					}
					$row = $prep->fetch();
				}
				catch (PDOException $e) {
					echo "<h1>Nu s-a reusit conectarea la baza de date: " . $e->getMessage() . "</h1>";
					exit;
				}
			?>
			<header>
				<h1>Profil - <?php echo $_GET["user"] ?></h1>
			</header>
	 		<div>
				<div class="tab-control" data-role="tab-control">
		    		<ul class="tabs">
		        		<li class="active"><a href="#profile">Profil</a></li>
		        		<?php if($ok) : ?>
		        			<li><a href="#edit">Edit</a></li>
		    			<?php endif; ?>
		    		</ul>

				    <div class="frames">
				        <div class="frame grid" id="profile">
				        	<div class="row">
				        		<div class="span2 offset1">
				        			<img src=
				        			<?php
				        				$file = "Uploads\\". $row["uid"] ."\\profile\\" . "profile_picture" . 
				        				$row["uid"] . ".jpg";
				        				if (is_file($file)) {
											echo $file;
										}
										else
				        					echo "public\\images\\user_generic.jpg";
				        			?>
				        				 class="rounded" style="display: inline block;"/>
				        		</div>
				        		<div class="span2">
				        			<h4 class="fg-gray"><strong>Nume:</strong></h4><br/>
				        			<?php 
				        				if ($row["study"])
				        					echo '<h4 class="fg-gray"><strong>Liceul/Facultate:</strong></h4><br/>';
				        				if ($row["city"])
				        					echo '<h4 class="fg-gray"><strong>Oras:</strong></h4><br/>';
				        				if ($row["birth_date"])
				        					echo '<h4 class="fg-gray"><strong>Data de nastere:</strong> </h4><br/>';
				        				if ($row["show_email"])
				        					echo '<h4 class="fg-gray"><strong>Email:</strong></h4><br/>';
				        				if ($row["description"])
				        					echo '<h4 class="fg-gray"><strong>Descriere:</strong></h4><br/>';
									?>
				        		</div>
				        		<div class="span7">
				        			<h4><?php echo $row["lastname"] . " " . $row["firstname"] ?></h4><br/>
				        			<?php 
				        				if ($row["study"])
				        					echo "<h4>" . $row["study"] . "</h4><br/>";
				        				if ($row["city"])
				        					echo "<h4>" . $row["city"] . "</h4><br/>";
				        				if ($row["birth_date"])
				        					echo "<h4>" . $row["birth_date"] . "</h4><br/>";
				        				if ($row["show_email"])
				        					echo "<h4>" . $row["email"] . "</h4><br/>";
				        				if ($row["description"])
				        					echo "<h4>" . $row["description"] . "</h4><br/>";
				        			?>
				        		</div>
				        	</div>
				        </div>
				        <?php if($ok) : ?>
					        <div class="frame" id="edit">
					        	<form id="modifyProfile" name="modifyProfile" method="post" enctype="multipart/form-data">
						        	<label>Nume</label>
						        	<div class="input-control text">
						        		<input id="lastname" type="text" name="lastname" value='<?php echo $row["lastname"]; ?>' placeholder="numele dvs."/>
						        	</div>
						        	<label>Prenume</label>
						        	<div class="input-control text">
						        		<input id="firstname" type="text" name="firstname" value='<?php echo $row["firstname"]; ?>' placeholder="prenumele dvs."/>
						        	</div>
						        	<label>Liceul/Facultate</label>
						        	<div class="input-control text">
						        		<input id="study" type="text" name="study" value='<?php if ($row["study"])
				        					echo $row["study"]; ?>' placeholder="liceul sau facultatea la care invatati in prezent"/>
						        	</div>
						        	<label>Oras</label>
						        	<div class="input-control text">
						        		<input id="city" type="text" name="city" value='<?php if ($row["city"])
				        					echo $row["city"]; ?>' placeholder="orasul in care locuiti"/>
						        	</div>
						        	<label>Arata Email</label>
								 	<div id="radio">
								    	<div class="input-control radio">
										    <label>
										        <input type="radio" name="radio" <?php if ($row["show_email"])
				        							echo 'checked="checked"'; ?> value="1"/>
										        <span class="check"></span>
										        Da
										    </label>
										</div>
										<div class="input-control radio">
										    <label>
										        <input type="radio" name="radio" <?php if ($row["show_email"] == 0)
				        							echo 'checked="checked"'; ?>  value="0"/>
										        <span class="check"></span>
										        Nu
										    </label>
										</div>	
								  	</div>
								  	<label>Poza profil</label>
								  	<div class="input-control file info-state">
	                					<input type="file" name="profilePicture"/>
	                					<button class="btn-file"></button>
	            					</div>
									<label>Descriere</label>
									<div class="input-control textarea">
								    	<textarea id="description" name="description" placeholder="o scurta descriere maxim 1000 de caractere"><?php if ($row["description"])
				        					echo $row["description"]; ?></textarea>
									</div>
									<input type="text" style="display: none" name="user" value='<?php echo $_SESSION["user"];?>'/>
									<button class="primary button" type="submit">Trimite</button>
								</form>
					        </div>
				    	<?php endif; ?>
						<script>
							$("form#modifyProfile").submit(function() {
							    $.Dialog({
							        shadow: true,
							        overlay: false,
							        icon: '<span class="icon-puzzle"></span>',
							        title: 'Actualizare profil',
							        width: 500,
							        padding: 10,
							        content: '<p id="result"></p><button class="primary button" onclick="refresh()">Ok</button>'
							    });

								var formData = new FormData($(this)[0]);

								$.ajax({
							        url: "modifyprofile.php",
							        type: 'POST',
							        data: formData,
							        async: true,
							        success: function (result) {
							        	$("#result").html(result);
							        },
							        cache: false,
							        contentType: false,
							        processData: false
							    });
							    return false;
							});

							var refresh = function () {
								$.Dialog.close();
								document.location = '<?php if ($_SERVER["QUERY_STRING"]) {  echo $_SERVER["PHP_SELF"] . "?" . $_SERVER["QUERY_STRING"]; } else { echo $_SERVER["PHP_SELF"]; } ?>'
							}
						</script>
				    </div>
				</div>
			</div>
		</div>
	</body>
</html>