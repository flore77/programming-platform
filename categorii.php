<?php include "session.php" ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Categorii Probleme Informatica</title>
		<?php include "include\\head.html"; ?>
	</head>
	<body class="metro">
		<?php include "include\\navbar.html" ?>
		<section class="metro container">
			<h1>Probleme de informatica pentru orice capitol</h1>
			<?php
				$hostdb = 'localhost';
				$namedb = 'infomania';
				$userdb = 'root';
				$passdb = '';
				try {
				 	$db = new PDO("mysql:host=$hostdb; dbname=$namedb", $userdb, $passdb);
					$result = $db->query("SELECT cid, category, problems FROM categories");
					if ($result === false) {
						echo "<h1>Ceva nu mers bine! Refresh!</h1>";
						exit;
					}
				}
				catch(PDOException $e) {
				  	echo "<h1>Nu s-a reusit conectarea la baza de date: " . $e->getMessage() . "</h1>";
					exit;
				}

			?>		
			<table class='table hovered'>
				<thead>
					<th>Categorie</th>
					<th>Nr. probleme</th>
				</thead>
				<tbody>
					<?php while($row = $result->fetch()) : ?>
						<tr class="clickableRow"
							href='<?php echo "probleme.php?cid=" . $row["cid"] . "&cat=" . $row["category"]?>'>
							<td><?php echo $row["category"] ?></td>
							<td><?php echo $row["problems"] ?></td>
						</tr>
					<?php endwhile; ?>
				</tbody>
			</table>
		</section>
		<script type="text/javascript" src="public/javascripts/clickableRow.js"></script>
	</body>
</html>