<?php
	$hostdb = 'localhost';
	$namedb = 'infomania';
	$userdb = 'root';
	$passdb = '';
	try {
		$db = new PDO("mysql:host=$hostdb; dbname=$namedb", $userdb, $passdb);
		$prep = $db->prepare("SELECT username FROM users WHERE username = :username");
		if ($prep->execute(array("username" => $_POST["username"])) === false) {
			echo "Ceva nu a mers bine, reincercati!";
			exit;
		}
		if($prep->rowCount() > 0) {
			header('HTTP/1.1 500 Username exists');
	        header('Content-Type: application/json; charset=UTF-8');
			exit;
		}
		$result = $db->query("SELECT usernr FROM info");
		$row = $result->fetch();
		$nr = $row["usernr"];
		date_default_timezone_set('Europe/Bucharest');
		$uid = date("d") . date("m") . date("y") . $row["usernr"];
		$result = $db->query("SELECT passphrase FROM info");
		$row = $result->fetch();
		$passwd = $_POST["password"] . $row["passphrase"];
		$prep = $db->prepare("INSERT INTO users(uid, type, username, password, lastname, firstname, email) VALUES 
			($uid, 'elev', :username, :password, :lastname, :firstname, :email)");
		if ($prep->execute(array("username" => $_POST["username"], "password" => md5($passwd), 
			"lastname" => $_POST["lastname"], "firstname" => $_POST["firstname"], "email" => $_POST["email"])) === false) {
			echo "Ceva nu a mers bine, reincercati!";
			exit;
		}
		$nr += 1;
		$db->query("UPDATE info SET usernr = $nr");
		echo "Te-ai inregistrat cu succes!";
	}
	catch (PDOException $e) {
		echo "Nu s-a reusit conectarea la baza de date: " . $e->getMessage();
		exit;
	}
	session_start();
	$_SESSION["user"] = $_POST["username"];
	$_SESSION["uid"] = $uid;
	setcookie("user", $_POST["username"], time() + (86400 * 1000), "/");
	setcookie("uid", $uid, time() + (86400 * 1000), "/");
?>