<?php include "session.php" ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Infomania Problema <?php if(isset($_GET["nume"])) echo $_GET["nume"]; else echo "Inexistenta"; ?></title>
		<?php include "include\\head.html" ?>
	</head>
	<body class="metro">
		<?php include "include\\navbar.html" ?>
		<section class="metro container">
			<?php
				if (!isset($_GET["pid"]) || !isset($_GET["nume"])) {
					echo "<h1>Nu ati selectat nicio problema valabila!</h1>";
					exit;
				}
				$hostdb = 'localhost';
				$namedb = 'infomania';
				$userdb = 'root';
				$passdb = '';
				try {
					$db = new PDO("mysql:host=$hostdb; dbname=$namedb", $userdb, $passdb);
					$prep = $db->prepare("SELECT pid, number, name, file, statement, input, output, restriction, 
					date_in, date_out, difficulty, category FROM problems LEFT JOIN categories
					ON problems.cid = categories.cid WHERE pid = :pid");
					if ($prep->execute(array("pid" => $_GET["pid"])) !== false) {
						if ($prep->rowCount() <= 0) {
							echo "<h1>Problema aleasa de dvs. nu exista!</h1>";
							exit;
						}
						else {
							$row = $prep->fetch();
						}
					}
					else {
						echo "<h1>Ceva nu a mers bine! Refresh!</h1>";
						exit;
					}

					if (isset($_SESSION["uid"])) {
						$prep = $db->prepare("SELECT score FROM users_problems WHERE uid = :uid && pid = :pid 
										ORDER BY date_time DESC");
						if ($prep->execute(array("uid" => $_SESSION["uid"], "pid" => $row["pid"])) === false) {
							echo "<h1>Problema aleasa de dvs. nu exista!</h1>";
							exit;
						}
						if ($prep->rowCount() > 0) {
							$score = $prep->fetch();
						}
					}
					
				}
				catch (PDOException $e) {
					echo "<h1>Nu s-a reusit conectarea la baza de date: " . $e->getMessage() . "</h1>";
					exit;
				}
			?>	
	        <h2 style="padding: 30px 0px 30px 0px"><strong><?php echo $row["name"] ?></strong></h2>
	        <table class="table bordered">
	            <thead>
	                <th>Categorie</th>
	                <th>Fisier intrare/iesire</th>
	                <th>Dificultate</th>
	                <th>Scorul tau</th>
	            </thead>
	            <tbody>
	                <tr>
	                    <td><?php echo $row["category"]; ?></td>
	                    <td><?php echo $row["file"]; ?>.in/<?php echo $row["file"]; ?>.out</td>
	                    <td>
	                    	<div id="difficulty" class="rating small">
				    			<ul>
				    				<?php for ($i = 0; $i < 5; $i++) {
				    				if ($row['difficulty'] != 0) {
				    					echo "<li class='rated'></li>";
				    					$row['difficulty']--;
				    				}
				    				else
				    					echo "<li></li>";
				    				} ?>
				    			</ul>
							</div>
						</td>
	        	        <td id="score"><?php if (isset($score["score"])) echo $score["score"]; else echo "n/a"; ?></td>
	                </tr>
	            </tbody>
	        </table>
	        <?php if (isset($_SESSION["uid"])) : ?>
	        	<a class="pull-right" href='<?php echo "solutii.php?pid=" . $row["pid"] . "&nume=" . $row["name"]; ?>'>
	        	Solutiile mele</a>
	        <?php endif; ?>
	        <div>
	            <p style="padding: 30px 0 30px 0"><?php echo $row["statement"]; ?></p>
	            <ul>
	                <li><p><strong>Date de intrare:</strong> <?php echo $row["input"]; ?></p></li>
	                <li><p><strong>Date de iesire:</strong> <?php echo $row["output"]; ?></p></li>
	                <li><p><strong>Restrictii:</strong> <?php echo $row["restriction"]; ?></p></li>
	            </ul>
	            <br><br>
	        </div>
	        <table class="table">
	            <thead>
	                <th><?php echo $row["file"]; ?>.in</th>
	                <th><?php echo $row["file"]; ?>.out</th>
	            </thead>
	            <tbody>
	                <tr>
	                    <td><?php echo $row["date_in"]; ?></td>
	                    <td><?php echo $row["date_out"]; ?></td>
	                </tr>
	            </tbody>
	        </table>
	        <form id="upload" method="post" enctype="multipart/form-data">
	            <div class="input-control file info-state pull-left" style="width: 300px; height: 30px;">
	                <input type="file" name="sourceUpload"/>
	                <button class="btn-file"></button>
	            </div>
	            <input type="text" style="display: none" name="pid" value="<?php echo $row['pid'] ?>"/>
	            <input type="text" style="display: none" name="file" value="<?php echo $row['file'] ?>"/>
	            <input type="text" style="display: none" name="uid" value="<?php echo $_SESSION['uid'] ?>"/>
	            <?php if (isset($_SESSION["user"]) && isset($_SESSION["uid"])) : ?>
	            	<button type="submit" style="margin-left: 20px; height: 30px;">Upload</button>
	       		<?php endif; ?>
	        </form>
	    	<?php if (!isset($_SESSION["user"]) && !isset($_SESSION["uid"])) : ?>
	        	<button class="disabled" style="margin-left: 20px; height: 30px; cursor: pointer !important;"
	        	onclick="notLogged()">Upload</button>
	        <?php endif; ?>
	    </section>
	    <script>
	    	var notLogged = function () {
	    		$.Dialog ({
	    			overlay: true,
					shadow: true,
					flat: true,
					icon: '<span class="icon-puzzle"></span>',
					title: "Neautentifcat",
					content:'<div style="padding: 30px; width: 300px; height: 100px;">' +
								'<p class="fg-red text-center"><strong>Nu sunteti autentificat!</strong></p><br/><br/>' +
								'<button class="button primary pull-left" onclick="Logged(1)">Autentifcare</button>' +
								'<button class="button primary pull-right" onclick="Logged(2)">Inregistrare</button>' +
							'</div>'
				});
	    	}

	    	var Logged = function (x) {
	    		if (x === 1) {
	    			$.Dialog.close();
	    			document.getElementById('logInScreen').click();
	    		}
	    		if (x === 2) {
	    			$.Dialog.close();
	    			document.getElementById('signUpScreen').click();
	    		}
	    	}

	    	$("form#upload").submit(function(){
	    		$.Dialog ({
	    			overlay: true,
					shadow: true,
					flat: true,
					icon: '<span class="icon-puzzle"></span>',
					title: "Problema",
					content: '<div style="padding: 30px 10px 10px; width: 500px; height: 400px;"><h2 id="printScore">' + 
					'Waiting...</h2><button class="button primary type="button" onclick="$.Dialog.close()">ok</button>' +
					'</div>'
				});

			    var formData = new FormData($(this)[0]);

			    $.ajax({
			        url: "verifyUploadSource.php",
			        type: 'POST',
			        data: formData,
			        async: true,
			        success: function (result) {
			        	if (isNaN(result)) {
			        		$("#score").html("0");
			        		$("#printScore").html(result);
			        	}
			        	else {
			        		$("#score").html(result);
			        		$("#printScore").html("Felicitari ati obtinut: " + result + " pct.");
			        	}
			        },
			        error: function (Xhr, textStatus, errorThrown) {
			        	$("#printScore").html(errorThrown);
			        },
			        cache: false,
			        contentType: false,
			        processData: false
			    });
			    return false;
			});
		</script>
</html>