<?php
	$hostdb = 'localhost';
	$namedb = 'infomania';
	$userdb = 'root';
	$passdb = '';

	try {
		$db = new PDO("mysql:host=$hostdb; dbname=$namedb", $userdb, $passdb);
		$result = $db->query("SELECT passphrase FROM info");
		$row = $result->fetch();
		$passwd = $_POST["password"] . $row["passphrase"];
		$prep = $db->prepare("SELECT uid,password FROM users WHERE username = :user");
		if($prep->execute(array("user" => $_POST["username"])) === false) {
			echo "Ceva nu a mers bine, reincercati!";
			exit;
		}
		if ($prep->rowCount() <= 0) {
			header('HTTP/1.1 500 Username exists');
	        header('Content-Type: application/json; charset=UTF-8');
	        exit;
		}
		$row = $prep->fetch();
		if (md5($passwd) === $row["password"]) {
			session_start();
			$_SESSION["user"] = $_POST["username"];
			$_SESSION["uid"] = $row["uid"];
			setcookie("user", $_POST["username"], time() + (86400 * 1000), "/");
			setcookie("uid", $row["uid"], time() + (86400 * 1000), "/");
		}
		else {
			header('HTTP/1.1 500 Username exists');
	        header('Content-Type: application/json; charset=UTF-8');
	        exit;
	        exit;
		}
	}
	catch (PDOException $e) {
		echo "<Nu s-a reusit conectarea la baza de date: " . $e->getMessage();
		exit;
	}
?>