<?php
$hostdb = 'localhost';
	$namedb = 'infomania';
	$userdb = 'root';
	$passdb = '';

	try {
		$db = new PDO("mysql:host=$hostdb; dbname=$namedb", $userdb, $passdb);
		$result = $db->query("SELECT passphrase FROM info");
		$row = $result->fetch();
		$passwd = $_POST["oldPassword"] . $row["passphrase"];
		$newPasswd = $_POST["newPassword"] . $row["passphrase"];
		$prep = $db->prepare("SELECT uid,password FROM users WHERE username = :user");
		if($prep->execute(array("user" => $_POST["username"])) === false) {
			echo "Ceva nu a mers bine, reincercati!";
			exit;
		}
		if ($prep->rowCount() <= 0) {
			echo "Ceva nu a mers bine, reincercati!";
			exit;
		}
		$row = $prep->fetch();
		if (md5($passwd) === $row["password"]) {
			$prep = $db->prepare("UPDATE users SET password = :password WHERE uid = :uid");
			if($prep->execute(array("password" => md5($newPasswd), "uid" => $row["uid"])) !== false)
				echo "Parola a fost salvata cu succes!";
			else
				echo "Ceva nu a mers bine, reincercati!";
		}
		else {
			header('HTTP/1.1 500 Password does not match!');
	        header('Content-Type: application/json; charset=UTF-8');
	        exit;
		}
	}
	catch (PDOException $e) {
		echo "<Nu s-a reusit conectarea la baza de date: " . $e->getMessage();
		exit;
	}
?>