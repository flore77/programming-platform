<?php 
	include "session.php";
	if (!isset($_SESSION["user"]) || !isset($_SESSION["uid"])) {
		echo "<h1>Pagina inexistenta!</h1>";
		exit;
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Infomania Solutie</title>
		<?php include "include\\head.html" ?>
	</head>
	<body class="metro">
		<?php include "include\\navbar.html" ?>
		<div class="metro container" name="principal">
			<?php
				if (!isset($_GET["sid"])) {
					echo "<h1>Nu ati selectat nicio solutie valabila!</h1>";
					exit;	
				}
				require_once("db.php");
				try {
					$db = new PDO("mysql:host=$hostdb; dbname=$namedb", $userdb, $passdb);
					$prep = $db->prepare("SELECT uid, users_problems.pid, date_time, score, test1, test2, test3, test4, test5, test6, test7, test8, test9, test10, message, users_problems.file, name FROM users_problems LEFT JOIN problems ON users_problems.pid = problems.pid WHERE sid = :sid");
					if ($prep->execute(array("sid" => $_GET["sid"])) === false) {
						echo "<h1>Ceva nu a mers bine, reincercati!</h1>";
						exit;
					}
					if ($prep->rowCount() <= 0) {
						echo "<h1>Solutie inexistenta!</h1>";
						exit;
					}
					$row = $prep->fetch();
					if ($_SESSION["uid"] !== $row["uid"]) {
						echo "<h1>Solutia nu va apartine!</h1>";
						exit;
					}
				}
				catch (PDOException $e) {
					echo "<h1>Nu s-a reusit conectarea la baza de date: " . $e->getMessage() . "</h1>";
					exit;
				}
			?>
			<h1>Solutia</h1>
			<table class="table">
				<thead>
					<th>Utilizator</th>
					<th>Problema</th>
					<th>Data</th>
					<th>Scor</th>
				</thead>
				<tr>
					<td><?php echo $_SESSION["user"] ?></td>
					<td><?php echo $row["name"]; ?></td>
					<td><?php echo $row["date_time"]; ?></td>
					<td><?php echo $row["score"]; ?></td>
				</tr>
			</table>
			<h2><strong>Evaluare</strong></h2>
			<p><?php echo $row["message"] ?></p>
			<table class="table bordered">
				<thead>
					<th>Test</th>
					<th>Scor</th>
					<?php $contor = 1; while ($contor <= 10) : ?>
						<tr>
							<td><?php echo "Test" . $contor; ?></td>
							<td><?php echo $row["test" . $contor]; ?></td>
						</tr>
					<?php $contor++; endwhile; ?>
				</thead>
			</table>
			<h2><strong>Cod</strong></h2>
			<?php
				$file = "Uploads\\" . $_SESSION["uid"] . "\\" . $row["pid"] . "\\" . $row["file"];
				$f = fopen($file, "r");
				$code = fread($f, filesize($file));
				echo nl2br($code);
			?>
		</div>
	</body>
</html>