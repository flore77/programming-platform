<?php include "session.php" ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Probleme Informatica <?php echo $_GET["cat"]; ?></title>
		<?php include "include\\head.html"; ?>
	</head>
	<body class="metro">
		<?php include "include\\navbar.html" ?>
		<div class="metro container">
			<?php
				if (!isset($_GET['cid']) || !isset($_GET["cat"])) {
					echo "<h1>Nu ati selectat nicio categorie valabila!</h1>";
					exit;
				}
				$hostdb = 'localhost';
				$namedb = 'infomania';
				$userdb = 'root';
				$passdb = '';
				try {
					$db = new PDO("mysql:host=$hostdb; dbname=$namedb", $userdb, $passdb);
					$prep = $db->prepare("SELECT pid,number,name,statement,difficulty FROM problems WHERE cid = :cid");
					if ($prep->execute(array("cid" => $_GET["cid"])) !== false) {
						if($prep->rowCount() <= 0) {
							echo "<h1>Nu exista nici o problema in categoria aleasa de dvs.!</h1>";
							exit;
						}
					}
					else {
						echo "<h1>Ceva nu mers bine! Refresh!</h1>";
					}
				}
				catch (PDOException $e) {
					echo "<h1>Nu s-a reusit conectarea la baza de date: " . $e->getMessage() . "</h1>";
					exit;
				}
			?>
			<h1>Probleme de informatica: <?php echo $_GET["cat"]; ?></h1>
			<table class="table hovered">
				<thead>
					<th>Numar</th>
					<th>Nume</th>
					<th>Enunt</th>
					<th>Dificultate</th>
				</thead>
				<tbody>
					<?php while($row = $prep->fetch()) :?>
						<tr class = "clickableRow" 
						href='<?php echo "problema.php?pid=" . $row["pid"] . "&nume=" . $row["name"]; ?>'>
							<td><?php echo $row["number"]; ?></a></td>
							<td><?php echo $row["name"]; ?></td>
							<td><?php echo $row["statement"]; ?></td>
							<td style="width: 120px">
		                    	<div id="difficulty" class="rating small">
					    			<ul><?php for ($i = 0; $i < 5; $i++) {
					    				if ($row['difficulty'] != 0) {
					    					echo "<li class='rated'></li>";
					    					$row['difficulty']--;
					    				}
					    				else
					    					echo "<li></li>";
					    			} ?>
								</div>
							</td>
						</tr>
					<?php endwhile; ?>
				</tbody>
			</table>
		</div>
		<script type="text/javascript" src="public/javascripts/clickableRow.js"></script>
	</body>
</html>